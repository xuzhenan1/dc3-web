import request from '@/config/axios'

export const pointValueApi = {
    list: (pointValue) => request({
        url: 'data_api/data/point_value/list',
        method: 'post',
        data: pointValue
    })
};
